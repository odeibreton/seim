﻿Imports System.Data.SqlClient

Public Class Form1

    Private Const connectionString = "Data Source=SEGUNDO150\SEGUNDO150;Initial Catalog=DaniDB;Integrated Security=True"

    Sub CalcularNecesidades()

        Const cmdText As String = "SELECT idProducto, Stockmin, Existencias FROM BD2.Productos WHERE Existencias < Stockmin"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        connection.Open()
        Dim reader = cmd.ExecuteReader()

        While reader.Read()

            Dim idProducto = reader.GetString(0)
            Dim stockMin = reader.GetInt16(1)
            Dim existencias = reader.GetInt16(2)

            Dim cantidad = stockMin - existencias

            AltaNecesidad(idProducto, cantidad)

        End While

        connection.Close()

    End Sub

    Function NuevoIdNecesidad() As String

        Const cmdText = "SELECT ISNULL(MAX(IdNecesidad), 0) + 1 FROM BD2.Necesidades"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        connection.Open()
        Dim resultado = cmd.ExecuteScalar()
        connection.Close()

        Return resultado

    End Function

    Sub AltaNecesidad(idProducto As String, cantidad As Short)

        Const cmdText As String = "INSERT INTO BD2.Necesidades (idNecesidad, idProducto, Cantidad) VALUES (@idNecesidad, @idProducto, @cantidad)"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        Dim idNecesidad = NuevoIdNecesidad()

        cmd.Parameters.AddWithValue("@idNecesidad", idNecesidad)
        cmd.Parameters.AddWithValue("@idProducto", idProducto)
        cmd.Parameters.AddWithValue("@cantidad", cantidad)

        connection.Open()
        cmd.ExecuteNonQuery()
        connection.Close()

    End Sub

    Sub ActualizarListView()

        Const cmdText = "SELECT idNecesidad, idProducto, Cantidad FROM BD2.Necesidades"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        ListViewDatos.Items.Clear()

        connection.Open()
        Dim reader = cmd.ExecuteReader()
        While reader.Read()

            Dim idNecesidad = reader.GetString(0)
            Dim idProducto = reader.GetString(1)
            Dim cantidad = reader.GetInt16(2)

            Dim item As New ListViewItem(idNecesidad)
            item.SubItems.Add(idProducto)
            item.SubItems.Add(cantidad)

            ListViewDatos.Items.Add(item)

        End While
        connection.Close()

    End Sub

    Private Sub BtnNecesidades_Click(sender As Object, e As EventArgs) Handles BtnNecesidades.Click

        CalcularNecesidades()

        ActualizarListView()

    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub ComprarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ComprarToolStripMenuItem.Click
        FormComprar.ShowDialog()
    End Sub

    Private Sub VenderToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VenderToolStripMenuItem.Click
        FormVender.ShowDialog()
    End Sub
End Class
