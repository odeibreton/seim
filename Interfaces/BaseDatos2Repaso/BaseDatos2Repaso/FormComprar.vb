﻿Imports System.Data.SqlClient

Public Class FormComprar

    Private Const connectionString = "Data Source=SEGUNDO150\SEGUNDO150;Initial Catalog=DaniDB;Integrated Security=True"

    Private Sub BtnBuscar_Click(sender As Object, e As EventArgs) Handles BtnBuscar.Click

        BuscarProducto()

    End Sub

    Private Sub BuscarProducto()

        Dim idProducto = TxtIdProducto.Text

        Const cmdText = "SELECT Descripcion, Precio, Stockmin, Existencias FROM BD2.Productos WHERE idProducto = @idProducto"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@idProducto", idProducto)

        connection.Open()
        Dim reader = cmd.ExecuteReader()
        If reader.Read() Then

            TxtDescripcion.Text = reader(0)
            TxtPrecio.Text = reader(1)
            TxtStockMin.Text = reader(2)
            TxtExistencias.Text = reader(3)

        Else

            MsgBox("Error")

            TxtDescripcion.Text = ""
            TxtPrecio.Text = ""
            TxtStockMin.Text = ""
            TxtExistencias.Text = ""

        End If
        connection.Close()

    End Sub

    Private Sub BtnComprar_Click(sender As Object, e As EventArgs) Handles BtnComprar.Click

        Dim cantidad As Short
        If Not Short.TryParse(TxtCantidad.Text, cantidad) Then
            Exit Sub
        End If

        Const cmdText = "UPDATE BD2.Productos SET Existencias += @cantidad WHERE idProducto = @idProducto"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@cantidad", cantidad)
        cmd.Parameters.AddWithValue("@idProducto", TxtIdProducto.Text)

    End Sub
End Class