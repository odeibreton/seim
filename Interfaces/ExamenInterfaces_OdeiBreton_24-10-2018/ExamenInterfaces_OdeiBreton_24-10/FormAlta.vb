﻿Public Class FormAlta
    Private Sub FormAlta_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LlenarCombo(CbCuerdas)

    End Sub

    Private Sub BtnAlta_Click(sender As Object, e As EventArgs) Handles BtnAlta.Click

        If TxtID.Text = String.Empty Or TxtNombre.Text = String.Empty Or CbCuerdas.SelectedItem = Nothing Then
            MsgBox("Todos los datos deben introducirse")
            Return
        End If

        Dim id As Integer = Integer.Parse(TxtID.Text)
        Dim nombre As String = TxtNombre.Text
        Dim voz As Cuerda = [Enum].Parse(GetType(Cuerda), CbCuerdas.SelectedItem)

        If coralistas.Contains(id.ToString()) Then
            MsgBox("La clave ya existe")
            Return
        End If

        coralistas.Add(Alta(id, nombre, voz), id)
        TxtID.Clear()
        TxtNombre.Clear()

    End Sub
End Class