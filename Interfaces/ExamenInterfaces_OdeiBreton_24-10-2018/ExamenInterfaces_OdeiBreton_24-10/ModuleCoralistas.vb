﻿Imports ExamenInterfaces_OdeiBreton_24_10

Module ModuleCoralistas

    Public coralistas As New Collection

    Public Enum Cuerda
        Tenor = 0
        Soprano
        Baritono
        Contralto
        Bajo
    End Enum

    Public Structure Coralista

        Private _id As Integer
        Private _nombre As String
        Private _convoado As Boolean
        Private _voz As Cuerda

        Public Property Id As Integer
            Get
                Return _id
            End Get
            Set(value As Integer)
                _id = value
            End Set
        End Property

        Public Property Nombre As String
            Get
                Return _nombre
            End Get
            Set(value As String)
                _nombre = value
            End Set
        End Property

        Public Property Convocado As Boolean
            Get
                Return _convoado
            End Get
            Set(value As Boolean)
                _convoado = value
            End Set
        End Property

        Friend Property Voz As Cuerda
            Get
                Return _voz
            End Get
            Set(value As Cuerda)
                _voz = value
            End Set
        End Property

        Public Sub SetConvocado(ByVal convocado As Boolean)
            Me.Convocado = convocado
        End Sub

    End Structure

    Public Function Alta(ByVal id As Integer, ByVal nombre As String,
                         ByVal voz As Cuerda, Optional ByVal convocado As Boolean = False) As Coralista

        Return New Coralista With {
            .Id = id,
            .Nombre = nombre,
            .Convocado = convocado,
            .Voz = voz
        }

    End Function

    Public Sub LlenarCombo(ByRef combo As ComboBox)

        Dim cuerdas = [Enum].GetNames(GetType(Cuerda))
        combo.Items.AddRange(cuerdas)

    End Sub

    Public Function NoConvocados(ByVal coleccion As Collection) As Integer()

        Dim lista As New List(Of Integer)

        For Each coralista As Coralista In coleccion
            If Not coralista.Convocado Then
                lista.Add(coralista.Id)
            End If
        Next

        Return lista.ToArray()

    End Function

    Public Sub LlenarListBox(ByRef lista As ListBox, ByRef ids As Integer())

        For Each id In ids
            lista.Items.Add(id)
        Next

    End Sub

    Public Sub Informe(ByVal coleccion As Collection)

        Console.WriteLine("{0,4} {1,20} {2,10}", "ID", "Nombre", "Cuerda")

        Dim cont As Integer = 0
        Dim contTenores As Integer = 0
        Dim contSopranos As Integer = 0
        Dim contBaritonos As Integer = 0
        Dim contContraltos As Integer = 0
        Dim contBajos As Integer = 0

        For Each coralista As Coralista In coleccion

            If coralista.Convocado Then

                Console.WriteLine("{0,4} {1,20} {2,10}", coralista.Id, coralista.Nombre, coralista.Voz.ToString())

                cont += 1

                Select Case coralista.Voz
                    Case Cuerda.Bajo
                        contBajos += 1
                    Case Cuerda.Baritono
                        contBaritonos += 1
                    Case Cuerda.Contralto
                        contContraltos += 1
                    Case Cuerda.Soprano
                        contSopranos += 1
                    Case Cuerda.Tenor
                        contSopranos += 1

                End Select

            End If

        Next

        Console.WriteLine("Total Convocados: {0}", cont)
        Console.WriteLine("Tenores: {0}", contTenores)
        Console.WriteLine("Sopranos: {0}", contSopranos)
        Console.WriteLine("Barítonos: {0}", contBaritonos)
        Console.WriteLine("Contraltos: {0}", contContraltos)
        Console.WriteLine("Bajos: {0}", contBajos)

    End Sub

End Module
