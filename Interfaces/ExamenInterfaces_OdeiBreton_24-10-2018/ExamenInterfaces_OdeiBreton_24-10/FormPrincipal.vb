﻿Public Class FormPrincipal

    Private Sub NuevoCoralistaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoCoralistaToolStripMenuItem.Click

        Dim formAlta As New FormAlta With {
            .MdiParent = Me
        }
        formAlta.Show()

    End Sub

    Private Sub ElaborarListaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ElaborarListaToolStripMenuItem.Click

        Dim formLista As New FormElaborarLista With {
            .MdiParent = Me
        }
        formLista.Show()

    End Sub

    Private Sub VerListaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerListaToolStripMenuItem.Click

        Informe(coralistas)

    End Sub
End Class
