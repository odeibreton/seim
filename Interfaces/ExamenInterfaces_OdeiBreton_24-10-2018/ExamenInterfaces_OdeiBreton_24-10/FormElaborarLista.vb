﻿Public Class FormElaborarLista
    Private Sub FormElaborarLista_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim ids As Integer() = NoConvocados(coralistas)
        LlenarListBox(LstIDs, ids)

    End Sub

    Private Sub LstIDs_Click(sender As Object, e As EventArgs) Handles LstIDs.Click

        If LstIDs.SelectedItems.Count > 0 Then

            Dim id As Integer = LstIDs.SelectedItem
            Dim coralista As Coralista = coralistas(id.ToString())

            TxtNombre.Text = coralista.Nombre
            TxtCuerda.Text = coralista.Voz.ToString()

        End If

    End Sub

    Private Sub BtnSeleccionar_Click(sender As Object, e As EventArgs) Handles BtnSeleccionar.Click

        If LstIDs.SelectedItems.Count > 0 Then

            Dim id As Integer = LstIDs.SelectedItem
            coralistas(id.ToString()).SetConvocado(True)

        End If

    End Sub

End Class