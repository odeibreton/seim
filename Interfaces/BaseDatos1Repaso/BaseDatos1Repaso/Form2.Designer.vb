﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnReintegro = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtReintegro = New System.Windows.Forms.TextBox()
        Me.txtDesde = New System.Windows.Forms.TextBox()
        Me.txtHasta = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnConsulta = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeaderMovimiento = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeaderImporte = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeaderConcepto = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeaderFecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LblNCuenta = New System.Windows.Forms.Label()
        Me.LblSaldo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnReintegro
        '
        Me.btnReintegro.Location = New System.Drawing.Point(25, 56)
        Me.btnReintegro.Name = "btnReintegro"
        Me.btnReintegro.Size = New System.Drawing.Size(75, 23)
        Me.btnReintegro.TabIndex = 0
        Me.btnReintegro.Text = "Reintegro"
        Me.btnReintegro.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nº Cuenta"
        '
        'txtReintegro
        '
        Me.txtReintegro.Location = New System.Drawing.Point(143, 59)
        Me.txtReintegro.Name = "txtReintegro"
        Me.txtReintegro.Size = New System.Drawing.Size(193, 20)
        Me.txtReintegro.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Location = New System.Drawing.Point(35, 144)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.Size = New System.Drawing.Size(100, 20)
        Me.txtDesde.TabIndex = 3
        '
        'txtHasta
        '
        Me.txtHasta.Location = New System.Drawing.Point(214, 144)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.Size = New System.Drawing.Size(100, 20)
        Me.txtHasta.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Desde"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(211, 111)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Hasta"
        '
        'btnConsulta
        '
        Me.btnConsulta.Location = New System.Drawing.Point(25, 189)
        Me.btnConsulta.Name = "btnConsulta"
        Me.btnConsulta.Size = New System.Drawing.Size(75, 23)
        Me.btnConsulta.TabIndex = 7
        Me.btnConsulta.Text = "Consultar"
        Me.btnConsulta.UseVisualStyleBackColor = True
        '
        'btnBorrar
        '
        Me.btnBorrar.Location = New System.Drawing.Point(153, 189)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(75, 23)
        Me.btnBorrar.TabIndex = 8
        Me.btnBorrar.Text = "Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeaderMovimiento, Me.ColumnHeaderImporte, Me.ColumnHeaderConcepto, Me.ColumnHeaderFecha})
        Me.ListView1.Location = New System.Drawing.Point(25, 233)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(370, 112)
        Me.ListView1.TabIndex = 9
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeaderMovimiento
        '
        Me.ColumnHeaderMovimiento.Text = "Movimiento"
        '
        'ColumnHeaderImporte
        '
        Me.ColumnHeaderImporte.Text = "Importe"
        '
        'ColumnHeaderConcepto
        '
        Me.ColumnHeaderConcepto.Text = "Concepto"
        '
        'ColumnHeaderFecha
        '
        Me.ColumnHeaderFecha.Text = "Fecha"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 367)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Saldo"
        '
        'LblNCuenta
        '
        Me.LblNCuenta.AutoSize = True
        Me.LblNCuenta.Location = New System.Drawing.Point(94, 23)
        Me.LblNCuenta.Name = "LblNCuenta"
        Me.LblNCuenta.Size = New System.Drawing.Size(0, 13)
        Me.LblNCuenta.TabIndex = 11
        '
        'LblSaldo
        '
        Me.LblSaldo.AutoSize = True
        Me.LblSaldo.Location = New System.Drawing.Point(65, 367)
        Me.LblSaldo.Name = "LblSaldo"
        Me.LblSaldo.Size = New System.Drawing.Size(0, 13)
        Me.LblSaldo.TabIndex = 12
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 417)
        Me.Controls.Add(Me.LblSaldo)
        Me.Controls.Add(Me.LblNCuenta)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.btnConsulta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtHasta)
        Me.Controls.Add(Me.txtDesde)
        Me.Controls.Add(Me.txtReintegro)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnReintegro)
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnReintegro As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtReintegro As TextBox
    Friend WithEvents txtDesde As TextBox
    Friend WithEvents txtHasta As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnConsulta As Button
    Friend WithEvents btnBorrar As Button
    Friend WithEvents ListView1 As ListView
    Friend WithEvents ColumnHeaderMovimiento As ColumnHeader
    Friend WithEvents ColumnHeaderImporte As ColumnHeader
    Friend WithEvents ColumnHeaderConcepto As ColumnHeader
    Friend WithEvents ColumnHeaderFecha As ColumnHeader
    Friend WithEvents Label4 As Label
    Friend WithEvents LblNCuenta As Label
    Friend WithEvents LblSaldo As Label
End Class
