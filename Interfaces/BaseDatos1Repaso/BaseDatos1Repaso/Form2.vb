﻿Imports System.Data.SqlClient

Public Class Form2

    Const connectionString As String = "Data Source=SEGUNDO150\SEGUNDO150;Initial Catalog=DaniDB;Integrated Security=True"

    Private nCuenta As String

    Public Sub New(nCuenta As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.nCuenta = nCuenta

    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ' Mostramos nCuenta
        LblNCuenta.Text = nCuenta

    End Sub

    Private Sub ObtenerSaldo()

        Const cmdText = "SELECT SaldoActual FROM BD1.Cuentas WHERE NCuenta = @nCuenta"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@nCuenta", nCuenta)

        connection.Open()
        Dim resultado = cmd.ExecuteScalar()
        connection.Close()

        LblSaldo.Text = resultado

    End Sub

    Private Sub BtnReintegro_Click(sender As Object, e As EventArgs) Handles btnReintegro.Click

        Const cmdText As String = "INSERT INTO BD1.Movimientos (NMovimiento, importe, Concepto, Fecha, NCuenta) VALUES (@nMovimiento, @importe, 'Odei es guay', GETDATE(), @nCuenta)"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        Dim nMovimiento = CalcularNMovimiento()
        Dim importe As Integer = txtReintegro.Text
        importe = -importe

        cmd.Parameters.AddWithValue("@nMovimiento", nMovimiento)
        cmd.Parameters.AddWithValue("@importe", importe)
        cmd.Parameters.AddWithValue("@nCuenta", nCuenta)

        connection.Open()
        cmd.ExecuteNonQuery()
        connection.Close()

        ActualizarSaldo()

        ActualizarListView()

    End Sub

    Private Function CalcularNMovimiento() As Integer

        Const cmdText As String = "SELECT ISNULL(MAX(NMovimiento) + 1, 1) FROM BD1.Movimientos"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        connection.Open()
        Dim nMovimiento = cmd.ExecuteScalar()
        connection.Close()

        Return nMovimiento

    End Function

    Sub ActualizarSaldo()

        Const cmdText As String = "UPDATE BD1.Cuentas SET SaldoActual = SaldoActual - @importe WHERE NCuenta = @nCuenta"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@nCuenta", nCuenta)
        cmd.Parameters.AddWithValue("@importe", txtReintegro.Text)

        connection.Open()
        cmd.ExecuteNonQuery()
        connection.Close()

    End Sub

    Sub ActualizarListView()

        Const cmdText = "SELECT NMovimiento, importe, Concepto, Fecha FROM BD1.Movimientos WHERE NCuenta = @nCuenta AND Fecha BETWEEN @fechaMin AND @fechaMax"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@nCuenta", nCuenta)
        cmd.Parameters.AddWithValue("@fechaMin", txtDesde.Text)
        cmd.Parameters.AddWithValue("@fechaMax", txtHasta.Text)

        ListView1.Items.Clear()

        connection.Open()
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read()

            Dim nMovimiento As String = reader.GetInt32(0)
            Dim importe As Decimal = reader.GetDecimal(1)
            Dim concepto As String = reader.GetString(2)
            Dim fecha As Date = reader.GetDateTime(3)

            Dim item As New ListViewItem
            item.Text = nMovimiento
            item.SubItems.Add(importe)
            item.SubItems.Add(concepto)
            item.SubItems.Add(fecha)

            ListView1.Items.Add(item)

        End While
        connection.Close()

        ObtenerSaldo()

    End Sub

    Private Sub BtnConsulta_Click(sender As Object, e As EventArgs) Handles btnConsulta.Click
        ActualizarListView()
    End Sub

    Private Sub BtnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click

        Dim resultado = MsgBox("Kiere vorrar", MsgBoxStyle.YesNo)

        ' Si la respuesta es sí
        If resultado = MsgBoxResult.Yes Then

            Const cmdText As String = "DELETE BD1.Movimientos WHERE Fecha BETWEEN @fechaMin AND @fechaMax"

            Dim connection As New SqlConnection(connectionString)
            Dim cmd As New SqlCommand(cmdText, connection)

            cmd.Parameters.AddWithValue("@fechaMin", txtDesde.Text)
            cmd.Parameters.AddWithValue("@fechaMax", txtHasta.Text)

            connection.Open()
            cmd.ExecuteNonQuery()
            connection.Close()

        End If

    End Sub
End Class
