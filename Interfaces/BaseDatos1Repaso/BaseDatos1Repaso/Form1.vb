﻿Imports System.Data.SqlClient

Public Class Form1

    Const connectionString As String = "Data Source=SEGUNDO150\SEGUNDO150;Initial Catalog=DaniDB;Integrated Security=True"
    Private Sub btnContinuar_Click(sender As Object, e As EventArgs) Handles btnContinuar.Click

        Const cmdText As String = "SELECT Username
                                    FROM BD1.Usuarios
                                    WHERE username= @username AND password=@password"

        Dim conexion As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, conexion)

        cmd.Parameters.AddWithValue("@username", txtUserName.Text)
        cmd.Parameters.AddWithValue("@password", txtPassword.Text)

        conexion.Open()
        Dim resultado As Object = cmd.ExecuteScalar()
        conexion.Close()

        If resultado Is Nothing Then
            'si el usuario no existe
            MsgBox("Password incorrecto")
        Else
            'Si el usuario existe
            RecuperarCuentas()
        End If

    End Sub

    Sub RecuperarCuentas()

        Const cmdText As String = "SELECT NCuenta, Tipo, SaldoActual
                                    FROM BD1.Cuentas
                                    WHERE titular = @titular"

        Dim conexion As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, conexion)

        cmd.Parameters.AddWithValue("@titular", txtUserName.Text)

        ListView1.Items.Clear()

        conexion.Open()
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read()
            'Recoger los datos para la listview
            Dim numCuenta = reader.GetString(0)
            Dim tipoCuenta = reader.GetString(1)
            Dim saldoActual = reader.GetDecimal(2)

            'Meter datos en la listview
            Dim item As New ListViewItem
            item.Text = numCuenta
            item.SubItems.Add(tipoCuenta)
            item.SubItems.Add(saldoActual)

            ListView1.Items.Add(item)


        End While
        conexion.Close()

    End Sub

    Private Sub ListView1_DoubleClick(sender As Object, e As EventArgs) Handles ListView1.DoubleClick

        ' Obtener nCuenta
        Dim nCuenta = ListView1.SelectedItems(0).Text

        ' Le pasamos nCuenta a Form2
        Dim formulario As New Form2(nCuenta)

        ' Mostrar form2
        formulario.ShowDialog()

    End Sub
End Class
