﻿Imports System.Data.SqlClient

Public Class FormAlquilar

    Private ReadOnly codLibro As Integer

    Private Const connectionString = "Data Source=ODEI-LAPTOP;Initial Catalog=DAM_OdeiBreton;Integrated Security=True"
    Private ReadOnly connection As New SqlConnection(connectionString)

    Sub New(codLibro As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.codLibro = codLibro

    End Sub

    Private Sub FormAlquilar_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        TxtFechaAlquiler.Text = Date.Today
        TxtFechaDevolucion.Text = Date.Today.AddDays(15)
        TxtCodLibro.Text = codLibro

    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click

        Dim cmdText = "INSERT INTO Prestamos
                    (CodLibro, CodSocio, FechaAlquiler, FechaDevolucion, Devuelto)
                    VALUES (@codLibro, @codSocio, @fechaAlquiler, @fechaDevolucion, 0)"

        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@codLibro", codLibro)
        cmd.Parameters.AddWithValue("@codSocio", TxtCodSocio.Text)
        cmd.Parameters.AddWithValue("@fechaAlquiler", TxtFechaAlquiler.Text)
        cmd.Parameters.AddWithValue("@fechaDevolucion", TxtFechaDevolucion.Text)

        connection.Open()
        cmd.ExecuteNonQuery()
        connection.Close()

        Close()

    End Sub
End Class