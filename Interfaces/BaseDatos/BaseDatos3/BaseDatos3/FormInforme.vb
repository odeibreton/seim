﻿Imports System.Data.SqlClient

Public Class FormInforme

    Private Const connectionString = "Data Source=ODEI-LAPTOP;Initial Catalog=DAM_OdeiBreton;Integrated Security=True"


    Private Sub BtnImprimir_Click(sender As Object, e As EventArgs) Handles BtnImprimir.Click

        Const cmdText = "SELECT CodSocio, CodLibro, FechaAlquiler, FechaDevolucion
                        FROM Prestamos
                        WHERE FechaAlquiler BETWEEN @fechaInicio AND @fechaFin
	                        AND Devuelto = 1"

        ' Creamos el DataSet
        Dim dataSet As New DataSet()

        ' Creamos el adaptador
        Dim adapter As New SqlDataAdapter(cmdText, connectionString)

        ' Crea la DELETE para poder borrar las filas de la base de datos
        Dim builder As New SqlCommandBuilder(adapter)
        adapter.DeleteCommand = builder.GetDeleteCommand()

        ' Configuramos los parámetros de la Select
        adapter.SelectCommand.Parameters.AddWithValue("@fechaInicio", TxtFechaInicio.Text)
        adapter.SelectCommand.Parameters.AddWithValue("@fechaFin", TxtFechaFin.Text)

        ' Llenamos el dataSet
        adapter.Fill(dataSet)

        ' Mostar datos por consola
        ' Mostrar cabecera
        Console.WriteLine("{0, 20} {1, 20} {2, 20} {3, 20}", "CodSocio", "CodLibro", "Fecha Alquiler", "Fecha Devolucion")

        ' Mostrar líneas
        ' Para eso, hay que recorrer las líneas de la tabla que hay en el DataSet
        ' Recorremos, dentro del DataSet, la tabla 0, las líneas
        For Each linea As DataRow In dataSet.Tables(0).Rows

            Dim codSocio = linea(0)
            Dim codLibro = linea(1)
            Dim fechaAlquiler = linea(2)
            Dim fechaDevolucion = linea(3)

            Console.WriteLine("{0, 20} {1, 20} {2, 20} {3, 20}",
                              codSocio, codLibro, fechaAlquiler, fechaDevolucion)

            ' Borramos la fila
            dataSet.Tables(0).Rows.Remove(linea)

        Next

        adapter.Update(dataSet)

        Close()

    End Sub

End Class