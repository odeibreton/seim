﻿Imports System.Data.SqlClient

Public Class Form1

    Private Const connectionString = "Data Source=ODEI-LAPTOP;Initial Catalog=DAM_OdeiBreton;Integrated Security=True"
    Private ReadOnly connection As New SqlConnection(connectionString)

    Private Sub BtnAlquilar_Click(sender As Object, e As EventArgs) Handles BtnAlquilar.Click

        Const cmdText = "SELECT Libros.CodLibro, Titulo, Autor, Editorial
                        FROM Libros
                        LEFT JOIN Prestamos
	                        ON Libros.CodLibro = Prestamos.CodLibro
                        WHERE Titulo = @titulo AND
                         (Devuelto IS NULL OR Devuelto = 0)"

        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@titulo", TxtTitulo.Text)

        ListViewLibros.Items.Clear()

        connection.Open()
        Dim reader = cmd.ExecuteReader()
        While reader.Read()

            Dim codLibro = reader.GetInt32(0)
            Dim titulo = reader.GetString(1)
            Dim autor = reader.GetString(2)
            Dim editorial = reader.GetString(3)

            Dim item As New ListViewItem()
            item.Text = codLibro
            item.SubItems.Add(titulo)
            item.SubItems.Add(autor)
            item.SubItems.Add(editorial)

            ListViewLibros.Items.Add(item)

        End While
        connection.Close()

    End Sub

    Private Sub ListViewLibros_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles ListViewLibros.MouseDoubleClick

        ' Obtenemos el código del libro
        Dim codLibro As Integer = ListViewLibros.SelectedItems(0).Text

        ' Mostramos el formulario
        Dim form As New FormAlquilar(codLibro)
        form.ShowDialog()

    End Sub

    Private Sub BtnInforme_Click(sender As Object, e As EventArgs) Handles BtnInforme.Click

        Dim form As New FormInforme()
        form.ShowDialog()

    End Sub
End Class
