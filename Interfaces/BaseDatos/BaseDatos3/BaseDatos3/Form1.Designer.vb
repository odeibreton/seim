﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtTitulo = New System.Windows.Forms.TextBox()
        Me.BtnAlquilar = New System.Windows.Forms.Button()
        Me.BtnDevolver = New System.Windows.Forms.Button()
        Me.BtnInforme = New System.Windows.Forms.Button()
        Me.ListViewLibros = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Título"
        '
        'TxtTitulo
        '
        Me.TxtTitulo.Location = New System.Drawing.Point(15, 29)
        Me.TxtTitulo.Name = "TxtTitulo"
        Me.TxtTitulo.Size = New System.Drawing.Size(138, 22)
        Me.TxtTitulo.TabIndex = 2
        '
        'BtnAlquilar
        '
        Me.BtnAlquilar.Location = New System.Drawing.Point(318, 12)
        Me.BtnAlquilar.Name = "BtnAlquilar"
        Me.BtnAlquilar.Size = New System.Drawing.Size(75, 23)
        Me.BtnAlquilar.TabIndex = 3
        Me.BtnAlquilar.Text = "Alquilar"
        Me.BtnAlquilar.UseVisualStyleBackColor = True
        '
        'BtnDevolver
        '
        Me.BtnDevolver.Location = New System.Drawing.Point(399, 12)
        Me.BtnDevolver.Name = "BtnDevolver"
        Me.BtnDevolver.Size = New System.Drawing.Size(75, 23)
        Me.BtnDevolver.TabIndex = 4
        Me.BtnDevolver.Text = "Devolver"
        Me.BtnDevolver.UseVisualStyleBackColor = True
        '
        'BtnInforme
        '
        Me.BtnInforme.Location = New System.Drawing.Point(399, 41)
        Me.BtnInforme.Name = "BtnInforme"
        Me.BtnInforme.Size = New System.Drawing.Size(75, 23)
        Me.BtnInforme.TabIndex = 5
        Me.BtnInforme.Text = "Informe"
        Me.BtnInforme.UseVisualStyleBackColor = True
        '
        'ListViewLibros
        '
        Me.ListViewLibros.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.ListViewLibros.Location = New System.Drawing.Point(12, 93)
        Me.ListViewLibros.Name = "ListViewLibros"
        Me.ListViewLibros.Size = New System.Drawing.Size(773, 345)
        Me.ListViewLibros.TabIndex = 6
        Me.ListViewLibros.UseCompatibleStateImageBehavior = False
        Me.ListViewLibros.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Código"
        Me.ColumnHeader1.Width = 101
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Título"
        Me.ColumnHeader2.Width = 132
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Autor"
        Me.ColumnHeader3.Width = 162
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Editorial"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.ListViewLibros)
        Me.Controls.Add(Me.BtnInforme)
        Me.Controls.Add(Me.BtnDevolver)
        Me.Controls.Add(Me.BtnAlquilar)
        Me.Controls.Add(Me.TxtTitulo)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TxtTitulo As TextBox
    Friend WithEvents BtnAlquilar As Button
    Friend WithEvents BtnDevolver As Button
    Friend WithEvents BtnInforme As Button
    Friend WithEvents ListViewLibros As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
End Class
