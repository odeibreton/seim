﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAlquilar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtCodSocio = New System.Windows.Forms.TextBox()
        Me.TxtCodLibro = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtFechaDevolucion = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtFechaAlquiler = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BtnAceptar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(34, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Código Cliente"
        '
        'TxtCodCliente
        '
        Me.TxtCodSocio.Location = New System.Drawing.Point(34, 64)
        Me.TxtCodSocio.Name = "TxtCodCliente"
        Me.TxtCodSocio.Size = New System.Drawing.Size(124, 22)
        Me.TxtCodSocio.TabIndex = 1
        '
        'TxtCodLibro
        '
        Me.TxtCodLibro.Location = New System.Drawing.Point(207, 64)
        Me.TxtCodLibro.Name = "TxtCodLibro"
        Me.TxtCodLibro.Size = New System.Drawing.Size(124, 22)
        Me.TxtCodLibro.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(207, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 17)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Código Libro"
        '
        'TxtFechaDevolucion
        '
        Me.TxtFechaDevolucion.Location = New System.Drawing.Point(207, 130)
        Me.TxtFechaDevolucion.Name = "TxtFechaDevolucion"
        Me.TxtFechaDevolucion.Size = New System.Drawing.Size(124, 22)
        Me.TxtFechaDevolucion.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(207, 110)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Fecha Devolucion"
        '
        'TxtFechaAlquiler
        '
        Me.TxtFechaAlquiler.Location = New System.Drawing.Point(34, 130)
        Me.TxtFechaAlquiler.Name = "TxtFechaAlquiler"
        Me.TxtFechaAlquiler.Size = New System.Drawing.Size(124, 22)
        Me.TxtFechaAlquiler.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(34, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 17)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Fecha Alquiler"
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Location = New System.Drawing.Point(146, 171)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.BtnAceptar.TabIndex = 8
        Me.BtnAceptar.Text = "Aceptar"
        Me.BtnAceptar.UseVisualStyleBackColor = True
        '
        'FormAlquilar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(423, 245)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.TxtFechaDevolucion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TxtFechaAlquiler)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TxtCodLibro)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TxtCodSocio)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormAlquilar"
        Me.Text = "FormDevolver"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TxtCodSocio As TextBox
    Friend WithEvents TxtCodLibro As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TxtFechaDevolucion As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TxtFechaAlquiler As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents BtnAceptar As Button
End Class
