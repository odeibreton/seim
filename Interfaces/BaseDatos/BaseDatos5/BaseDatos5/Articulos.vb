﻿Imports System.Data.SqlClient

Public Class Articulos

    Const connectionString As String = "Data Source=ODEI-LAPTOP;Initial Catalog=DAM_OdeiBreton;Integrated Security=True"
    Private Sub Articulos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim IdArticulo = InputBox("Introduce un Id de Artículo")

        'Mostrará en la etiqueta [Id Articulo] el código introducido
        lblIdArticulo.Text = IdArticulo

        If ExisteArticulo(IdArticulo) Then
            'Existe el articulo
            btnAlta.Enabled = True
            txtDescripcion.Enabled = False
            txtExistencias.Enabled = False
            txtPrecio.Enabled = False

        Else
            btnModificacion.Enabled = True
            btnBaja.Enabled = True


        End If

    End Sub

    'Para comprobar si existe un artículo
    Function ExisteArticulo(id As Integer) As Boolean

        'Recorre todas las filas
        For Each fila As DS_BaseDatos5.ArticulosRow In DS_BaseDatos51.Articulos.Rows

            'Si el IdArticulo de la fila =id
            If fila.IdArticulo = id Then

                'Devuelve que exite
                Return True
            End If
        Next

        'Si no lo encuentra, devuelve false
        Return False

    End Function

End Class