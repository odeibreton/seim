﻿Imports System.Data.SqlClient

Public Class Form1

    Private Const connectionString As String = "{String de conexión}"

    Private Sub BtnContinuar_Click(sender As Object, e As EventArgs) Handles BtnContinuar.Click

        Const cmdText As String = "SELECT Username FROM BD1.Usuarios WHERE Username = @username AND Password = @password"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@username", TxtUsername.Text)
        cmd.Parameters.AddWithValue("@password", TxtPassword.Text)

        connection.Open()
        Dim resultado As Object = cmd.ExecuteScalar()
        connection.Close()

        If resultado Is Nothing Then
            ' El usuario no existe

            MsgBox("El usuario no existe")
        Else
            ' El usuario existe
            ObtenerCuentas()

        End If

    End Sub

    Sub ObtenerCuentas()

        Const cmdText = "Comando"

        Dim connection As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand(cmdText, connection)

        cmd.Parameters.AddWithValue("@titular", TxtUsername.Text)

        connection.Open()

        connection.Close()

    End Sub

End Class
