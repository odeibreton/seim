﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LecturaEscritura
{
    class Reader
    {
        private readonly ListView _listView;
        private readonly Pizarra _pizarra;

        private Thread _thread;

        public Reader(ListView listView, Pizarra pizarra)
        {
            _listView = listView ?? throw new ArgumentNullException(nameof(listView));
            _pizarra = pizarra ?? throw new ArgumentNullException(nameof(pizarra));

            _thread = new Thread(Run);
        }

        public void Start()
        {
            if (!_thread.IsAlive)
            {
                _thread = new Thread(Run);
                _thread.Start();
            }
        }

        private void Run()
        {
            _pizarra.GetSharedAccess();
            for (int index = 0; index < Pizarra._dataLength; index++)
            {
                int value = _pizarra[index];
                _listView.Invoke(new EditListView(UpdateListView), new object[] { _listView, index, value });

                Thread.Sleep(50);
            }
            _pizarra.ReleaseSharedAccess();
        }

        private void UpdateListView(ListView listView, int position, int value)
        {
            ListViewItem item = new ListViewItem
            {
                Text = position.ToString(),
                SubItems =
                {
                    value.ToString()
                }
            };

            listView.Items.Add(item);
        }
    }
}
