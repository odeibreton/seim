﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LecturaEscritura
{
    class Pizarra
    {
        public const int _dataLength = 10;

        private readonly int[] _data = new int[_dataLength];
        private int _readerCount = 0;

        public void Write(int position, int value)
        {
            _data[position] = value;
        }

        public int Read(int position)
        {
            return _data[position];
        }

        public int this[int index]
        {
            get => Read(index);
            set => Write(index, value);
        }

        public void GetExclusiveAccess()
        {
            lock (this)
            {
                while (_readerCount != 0)
                {
                    Monitor.Wait(this);
                }

                _readerCount--;
            }
        }

        public void ReleaseExclusiveAccess()
        {
            lock (this)
            {
                _readerCount = 0;
                Monitor.PulseAll(this);
            }
        }

        public void GetSharedAccess()
        {
            lock (this)
            {
                while (_readerCount < 0)
                {
                    Monitor.Wait(this);
                }

                _readerCount++;
            }
        }

        public void ReleaseSharedAccess()
        {
            lock (this)
            {
                _readerCount--;
                Monitor.PulseAll(this);
            }
        }
    }
}
