﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LecturaEscritura
{
    class Writer
    {
        private readonly Pizarra _pizarra;
        private readonly Thread _thread;

        private State _state = State.Stopped;
        private bool _hasData = false;
        private int _position, _value;

        public Writer(Pizarra pizarra)
        {
            _pizarra = pizarra ?? throw new ArgumentNullException(nameof(pizarra));

            _thread = new Thread(Run);
        }

        public void Write(int index, int value)
        {
            _position = index;
            _value = value;

            _hasData = true;
        }

        public int this[int index]
        {
            set => Write(index, value);
        }

        public void Start()
        {
            if (!_thread.IsAlive)
            {
                _state = State.Started;
                _thread.Start();
            }
        }

        public void Stop()
        {
            if (_thread.IsAlive)
            {
                _state = State.Stopping;
                _thread.Join();
            }
        }

        private void Run()
        {
            while (_state != State.Stopping)
            {
                if (_hasData)
                {
                    _pizarra.GetExclusiveAccess();
                    _pizarra[_position] = _value;
                    _hasData = false;

                    _pizarra.ReleaseExclusiveAccess();
                }

                Thread.Sleep(50);
            }
        }
    }

    enum State
    {
        Starting,
        Started,
        Stopping,
        Stopped
    }
}
