﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LecturaEscritura
{
    public partial class Form1 : Form
    {
        private readonly Pizarra _pizarra;
        private readonly Writer _writer;
        private readonly Reader _reader;

        public Form1()
        {
            InitializeComponent();

            _pizarra = new Pizarra();
            _writer = new Writer(_pizarra);
            _reader = new Reader(listViewDatos, _pizarra);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _writer.Start();
        }

        private void BtnLeer_Click(object sender, EventArgs e)
        {
            listViewDatos.Items.Clear();
            _reader.Start();
        }

        private void BtnEscribir_Click(object sender, EventArgs e)
        {
            if (int.TryParse(txtPosicion.Text, out int position) && int.TryParse(txtValor.Text, out int value))
            {
                _writer[position] = value;
            }
        }

        private void btnPararEscritor_Click(object sender, EventArgs e)
        {
            _writer.Stop();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _writer.Stop();
        }
    }

    public delegate void EditListView(ListView listView, int position, int value);
}
