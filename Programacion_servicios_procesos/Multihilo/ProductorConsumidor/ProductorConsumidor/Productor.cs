﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductorConsumidor
{
    class Productor
    {
        private readonly Mutex _mutex;
        private readonly Thread _thread;
        private readonly Buffer _buffer;
        private readonly int _min;
        private readonly int _max;

        public Productor(int min, int max, Buffer buffer)
        {
            _buffer = buffer;
            _min = min;
            _max = max;

            _mutex = new Mutex(true);

            _thread = new Thread(new ThreadStart(Produccion))
            {
                Name = "Productor"
            };
        }

        public void Empezar()
        {
            if (!_thread.IsAlive)
                _thread.Start();
        }

        public void Producir()
        {
            lock (this)
                Monitor.PulseAll(this);
        }

        private void Produccion()
        {
            for (int pos = _min; pos < _max; pos++)
            {
                lock (this)
                {
                    Monitor.Wait(this);
                    _buffer.Escribir(pos);
                }
            }
        }
    }
}
