﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductorConsumidor
{
    class Consumidor
    {
        private readonly int _max;
        private readonly Buffer _buffer;
        private readonly Thread _thread;
        private readonly Mutex _mutex;

        public Consumidor(int max, Buffer buffer)
        {
            _max = max;
            _buffer = buffer;

            _mutex = new Mutex(true);

            _thread = new Thread(new ThreadStart(Consumo))
            {
                Name = "Consumidor"
            };
        }

        public void Empezar()
        {
            if (!_thread.IsAlive)
                _thread.Start();
        }

        public void Consumir()
        {
            lock (this)
                Monitor.PulseAll(this);
        }

        private void Consumo()
        {
            for (int i = 0; i < _max; i++)
            {
                lock (this)
                {
                    Monitor.Wait(this);
                    _buffer.Leer();
                }
            }
        }
    }
}
