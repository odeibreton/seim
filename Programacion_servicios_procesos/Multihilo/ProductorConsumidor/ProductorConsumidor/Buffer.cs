﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductorConsumidor
{
    class Buffer
    {
        private int dato = -1;

        public void Escribir(int x)
        {
            lock (this)
            {
                while (dato != -1)
                {
                    Console.WriteLine("{0} bloqueado. Buffer Lleno", Thread.CurrentThread.Name);
                    Monitor.Wait(this);
                }

                Console.WriteLine("{0} escribió {1}", Thread.CurrentThread.Name, x.ToString());
                dato = x;
                Monitor.PulseAll(this);
            }
        }

        public int Leer()
        {
            lock (this)
            {
                while (dato == -1)
                {
                    Console.WriteLine("{0} bloqueado. Buffer Vacío", Thread.CurrentThread.Name);
                    Monitor.Wait(this);
                }
                Console.WriteLine("{0} Leyó {1}", Thread.CurrentThread.Name, dato.ToString());
                int v = dato;
                dato = -1;
                Monitor.PulseAll(this);
                return v;
            }
        }
    }
}
