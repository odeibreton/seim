﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductorConsumidor
{
    public partial class Form1 : Form
    {
        private readonly Buffer _buffer;
        private readonly Productor _productor;
        private readonly Consumidor _consumidor;

        public Form1()
        {
            InitializeComponent();

            _buffer = new Buffer();
            _productor = new Productor(0, 100, _buffer);
            _consumidor = new Consumidor(100, _buffer);

            _consumidor.Empezar();
            _productor.Empezar();

            Thread thread1 = new Thread(AvanzarPB1);
            Thread thread2 = new Thread(AvanzarPB2);

            thread1.Start();
            thread2.Start();
        }

        delegate void Interact(ProgressBar progressBar, Action<ProgressBar> action);

        void AvanzarPB1()
        {
            while (true)
            {
                // TODO: Terminar
                progressBar1.PerformStep();

                if (progressBar1.Value == progressBar1.Maximum)
                {
                    progressBar1.Value = progressBar1.Minimum;

                    _productor.Producir();
                }
            }
        }

        void AvanzarPB2()
        {
            while (true)
            {
                progressBar2.PerformStep();

                if (progressBar2.Value == progressBar2.Maximum)
                {
                    progressBar2.Value = progressBar2.Minimum;

                    _consumidor.Consumir();
                }
            }
        }
    }
}
