﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SeimSockets.UnitTests
{
    public class ClientHandlerTests
    {
        private const string hostname = "localhost";
        private const int port = 4446;

        private ClientHandler GetClientHandler(Client client, Server server)
        {
            ClientHandler sut = null;

            using (Task<ClientHandler> task = server.ListenAsync())
            {
                client.Connect();
                task.Wait();
                sut = task.Result;
            }

            return sut;
        }

        [Fact]
        void ShouldBeConnected()
        {
            Client client = new Client(hostname, port);
            Server server = new Server(port);

            var sut = GetClientHandler(client, server);

            bool connected = sut.Connected;

            server.Close(sut);
            client.Close();

            server.Dispose();
            client.Dispose();

            Assert.True(connected);
        }

        [Fact]
        void ShouldBeDisconnected()
        {
            Client client = new Client(hostname, port);
            Server server = new Server(port);

            var sut = GetClientHandler(client, server);

            server.Close(sut);
            client.Close();

            bool connected = sut.Connected;

            server.Dispose();
            client.Dispose();

            Assert.False(connected);
        }
    }
}
