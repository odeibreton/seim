﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SeimSockets.UnitTests
{
    public class ClientTests
    {
        private const string hostname = "localhost";
        private const int port = 4444;

        #region Connect

        [Fact]
        void ShouldConnect()
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            using (Task task = server.ListenAsync())
            {
                sut.Connect();
                task.Wait();
            }

            bool actual = sut.Connected;

            sut.Close();

            server.Dispose();
            sut.Dispose();

            Assert.True(actual);
        }

        [Fact]
        void ShouldTryConnect_True()
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            bool tryResult;
            using (Task task = server.ListenAsync())
            {
                tryResult = sut.TryConnect();
                task.Wait();
            }

            bool connected = sut.Connected;

            sut.Close();

            server.Dispose();
            sut.Dispose();

            Assert.True(tryResult);
            Assert.True(connected);
        }

        [Fact]
        void ShouldTryConnect_False()
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            bool tryResult = sut.TryConnect();
            bool connected = sut.Connected;

            sut.Close();

            server.Dispose();
            sut.Dispose();

            Assert.False(tryResult);
            Assert.False(connected);
        }

        [Fact]
        void ShouldConnectAsync()
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            using (Task taskServer = server.ListenAsync())
            {
                using (Task taskSut = sut.ConnectAsync())
                {
                    taskSut.Wait();
                }

                taskServer.Wait();
            }

            bool actual = sut.Connected;

            sut.Close();

            server.Dispose();
            sut.Dispose();

            Assert.True(actual);
        }

        [Fact]
        void ShouldTryConnectAsync_True()
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            bool tryResult;
            using (Task taskServer = server.ListenAsync())
            {
                using (Task<bool> taskSut = sut.TryConnectAsync())
                {
                    taskSut.Wait();
                    tryResult = taskSut.Result;
                }

                taskServer.Wait();
            }

            bool connected = sut.Connected;

            sut.Close();

            server.Dispose();
            sut.Dispose();

            Assert.True(tryResult);
            Assert.True(connected);
        }

        [Fact]
        void ShouldTryConnectAsync_False()
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            bool tryResult;
            using (Task<bool> task = sut.TryConnectAsync())
            {
                task.Wait();
                tryResult = task.Result;
            }
            bool connected = sut.Connected;

            sut.Close();

            server.Dispose();
            sut.Dispose();

            Assert.False(tryResult);
            Assert.False(connected);
        }

        #endregion

        #region Close

        [Fact]
        void ShouldClose()
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            using (Task task = server.ListenAsync())
            {
                sut.Connect();
                task.Wait();
            }

            sut.Close();

            bool actual = sut.Connected;

            server.Dispose();
            sut.Dispose();

            Assert.False(actual);
        }

        #endregion

        #region Read

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldRead(string expected)
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task<ClientHandler> task = server.ListenAsync())
            {
                sut.Connect();
                task.Wait();

                clientHandler = task.Result;
            }

            server.WriteToClient(clientHandler, expected);
            string actual = sut.Read();

            sut.Close();
            server.Close(clientHandler);

            server.Dispose();
            sut.Dispose();

            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldReadAsync(string expected)
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task<ClientHandler> task = server.ListenAsync())
            {
                sut.Connect();
                task.Wait();

                clientHandler = task.Result;
            }

            string actual = null;

            server.WriteToClient(clientHandler, expected);
            using (Task<string> task = sut.ReadAsync())
            {
                task.Wait();
                actual = task.Result;
            }

            sut.Close();
            server.Close(clientHandler);

            server.Dispose();
            sut.Dispose();

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Write

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldWrite(string expected)
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task<ClientHandler> task = server.ListenAsync())
            {
                sut.Connect();
                task.Wait();

                clientHandler = task.Result;
            }

            sut.Write(expected);
            string actual = server.Read(clientHandler);

            sut.Close();
            server.Close(clientHandler);

            server.Dispose();
            sut.Dispose();

            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldWriteAsync(string expected)
        {
            var server = new Server(port);
            var sut = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task<ClientHandler> task = server.ListenAsync())
            {
                sut.Connect();
                task.Wait();

                clientHandler = task.Result;
            }

            using (Task task = sut.WriteAsync(expected))
            {
                task.Wait();
            }
            string actual = server.Read(clientHandler);

            sut.Close();
            server.Close(clientHandler);

            server.Dispose();
            sut.Dispose();

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Write/Read Data

        public static List<object[]> ReadWriteData = new List<object[]>()
        {
            new []{ "Short" },
            new []{ "Loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong" },
            new []{ "spécïal ñ" },
            new []{ "" }
        };

        #endregion
    }
}
