﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Linq;

namespace SeimSockets.UnitTests
{
    public class ServerTests
    {
        private const string hostname = "localhost";
        private const int port = 4445;

        #region Listen

        [Fact]
        void ShouldListen()
        {
            Server sut = new Server(port);
            Client client = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task task = client.ConnectAsync())
            {
                clientHandler = sut.Listen();
                task.Wait();
            }

            bool connected = clientHandler.Connected;

            sut.Close(clientHandler);
            client.Close();

            sut.Dispose();
            client.Dispose();

            Assert.True(connected);
        }

        [Fact]
        void ShouldListenAsync()
        {
            Server sut = new Server(port);
            Client client = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task taskClient = client.ConnectAsync())
            {
                using (Task<ClientHandler> taskSut = sut.ListenAsync())
                {
                    taskSut.Wait();
                    clientHandler = taskSut.Result;
                }

                taskClient.Wait();
            }

            bool connected = clientHandler.Connected;

            sut.Close(clientHandler);
            client.Close();

            sut.Dispose();
            client.Dispose();

            Assert.True(connected);
        }

        #endregion

        #region Close

        [Fact]
        void ShouldClose()
        {
            Server sut = new Server(port);
            Client client = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task task = client.ConnectAsync())
            {
                clientHandler = sut.Listen();
                task.Wait();
            }

            sut.Close(clientHandler);
            client.Close();

            bool connected = clientHandler.Connected;

            sut.Dispose();
            client.Dispose();

            Assert.False(connected);
        }

        #endregion

        #region Read

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldRead(string expected)
        {
            Server sut = new Server(port);
            Client client = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task task = client.ConnectAsync())
            {
                clientHandler = sut.Listen();
                task.Wait();
            }

            client.Write(expected);
            string actual = sut.Read(clientHandler);

            sut.Close(clientHandler);
            client.Close();

            sut.Dispose();
            client.Dispose();

            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldReadAsync(string expected)
        {
            Server sut = new Server(port);
            Client client = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task task = client.ConnectAsync())
            {
                clientHandler = sut.Listen();
                task.Wait();
            }

            client.Write(expected);
            string actual = null;
            using (Task<string> task = sut.ReadAsync(clientHandler))
            {
                task.Wait();
                actual = task.Result;
            }

            sut.Close(clientHandler);
            client.Close();

            sut.Dispose();
            client.Dispose();

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Write

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldWriteToClient(string expected)
        {
            Server sut = new Server(port);
            Client client = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task task = client.ConnectAsync())
            {
                clientHandler = sut.Listen();
                task.Wait();
            }

            sut.WriteToClient(clientHandler, expected);
            string actual = client.Read();

            sut.Close(clientHandler);
            client.Close();

            sut.Dispose();
            client.Dispose();

            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldWriteToClientAsync(string expected)
        {
            Server sut = new Server(port);
            Client client = new Client(hostname, port);

            ClientHandler clientHandler = null;

            using (Task task = client.ConnectAsync())
            {
                clientHandler = sut.Listen();
                task.Wait();
            }

            sut.WriteToClient(clientHandler, expected);
            string actual = null;
            using (Task<string> task = client.ReadAsync())
            {
                task.Wait();
                actual = task.Result;
            }

            sut.Close(clientHandler);
            client.Close();

            sut.Dispose();
            client.Dispose();

            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldWriteToAll(string expected)
        {
            Server sut = new Server(port);
            Client[] clients = new Client[]
            {
                new Client(hostname, port),
                new Client(hostname, port),
                new Client(hostname, port),
                new Client(hostname, port)
            };
            ClientHandler[] handlers = new ClientHandler[clients.Length];
            string[] results = new string[clients.Length];

            for (int i = 0; i < clients.Length; i++)
            {
                using (Task<ClientHandler> task = sut.ListenAsync())
                {
                    clients[i].Connect();
                    task.Wait();
                    handlers[i] = task.Result;
                }
            }

            sut.WriteToAll(expected);
            for (int i = 0; i < handlers.Length; i++)
            {
                results[i] = clients[i].Read();
            }

            sut.CloseAll();
            foreach (var client in clients)
                client.Close();

            sut.Dispose();
            foreach (var client in clients)
                client.Dispose();

            Assert.All(results, actual => Assert.Equal(expected, actual));
        }

        [Theory]
        [MemberData(nameof(ReadWriteData))]
        void ShouldWriteToAllAsync(string expected)
        {
            Server sut = new Server(port);
            Client[] clients = new Client[]
            {
                new Client(hostname, port),
                new Client(hostname, port),
                new Client(hostname, port),
                new Client(hostname, port)
            };
            ClientHandler[] handlers = new ClientHandler[clients.Length];
            string[] results = new string[clients.Length];

            for (int i = 0; i < clients.Length; i++)
            {
                using (Task<ClientHandler> task = sut.ListenAsync())
                {
                    clients[i].Connect();
                    task.Wait();
                    handlers[i] = task.Result;
                }
            }

            using (Task task = sut.WriteToAllAsync(expected))
            {
                task.Wait();
            }

            for (int i = 0; i < handlers.Length; i++)
            {
                results[i] = clients[i].Read();
            }

            sut.CloseAll();
            foreach (var client in clients)
                client.Close();

            sut.Dispose();
            foreach (var client in clients)
                client.Dispose();

            Assert.All(results, actual => Assert.Equal(expected, actual));
        }

        #endregion

        #region Write/Read Data

        public static List<object[]> ReadWriteData = new List<object[]>()
        {
            new []{ "Short" },
            new []{ "Loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong" },
            new []{ "spécïal ñ" },
            new []{ "" }
        };

        #endregion
    }
}
