﻿using System;
using System.Threading.Tasks;
using SeimSockets;

namespace SeimSockets.Startup.Core
{
    class Program
    {
        private const string Hostname = "localhost";
        private const int Port = 4444;

        static void Main(string[] args)
        {
            Client client = new Client("192.168.37.150", 5555);
            client.Connect();

            Console.ReadLine();
        }
    }
}
