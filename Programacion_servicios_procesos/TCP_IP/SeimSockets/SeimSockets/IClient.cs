﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeimSockets
{
    public interface IClient
    {
        string Hostname { get; }
        int Port { get; }
        bool Connected { get; }

        void Connect();
        bool TryConnect();
        Task ConnectAsync();
        Task<bool> TryConnectAsync();

        void Write(string message);
        Task WriteAsync(string message);

        void Close();

        string Read();
        Task<string> ReadAsync();
    }
}
