﻿using System;
using System.IO;
using System.Net.Sockets;

namespace SeimSockets
{
    public class ClientHandler : IDisposable
    {
        private readonly WeakReference<TcpClient> tcpClient;

        #region Reader/Writer

        private StreamWriter _writer;
        internal StreamWriter Writer
        {
            get
            {
                if (_writer is null)
                {
                    lock (this)
                    {
                        if (_writer is null)
                        {
                            _writer = new StreamWriter(GetClient().GetStream());
                        }
                    }
                }

                return _writer;
            }
        }

        private StreamReader _reader;
        internal StreamReader Reader
        {
            get
            {
                if (_reader is null)
                {
                    lock (this)
                    {
                        if (_reader is null)
                        {
                            _reader = new StreamReader(GetClient().GetStream());
                        }
                    }
                }

                return _reader;
            }
        }

        #endregion

        internal ClientHandler(TcpClient tcpClient)
        {
            if (tcpClient is null)
                throw new ArgumentNullException(nameof(tcpClient));

            this.tcpClient = new WeakReference<TcpClient>(tcpClient);
        }

        public bool Connected => GetVerifiedClient().Connected;

        internal void Close() => GetClient()?.Close();

        private TcpClient GetClient()
        {
            if (tcpClient.TryGetTarget(out TcpClient client))
            {
                return client;
            }

            return null;
        }

        private TcpClient GetVerifiedClient()
        {
            var client = GetClient();

            if (client == null)
                throw new NullReferenceException("The connection to the client is not aviable.");

            return client;
        }

        #region Operators

        public static implicit operator ClientHandler(TcpClient tcpClient) => new ClientHandler(tcpClient);

        #endregion

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    GetClient()?.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
