﻿using System;
using System.Collections.ObjectModel;
using System.Net.Sockets;

namespace SeimSockets
{
    public partial class Server
    {
        public class ClientCollection : Collection<ClientHandler>, IDisposable
        {
            #region IDisposable Support
            private bool disposedValue = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        foreach (var item in Items)
                        {
                            item.Dispose();
                        }
                    }

                    disposedValue = true;
                }
            }

            public void Dispose()
            {
                Dispose(true);
            }
            #endregion
        }
    }
}
