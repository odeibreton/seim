﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SeimSockets
{
    public class Client : IClient, IDisposable
    {
        private readonly TcpClient tcpClient = new TcpClient();

        #region Reader/Writer

        private StreamWriter _writer;
        internal StreamWriter Writer
        {
            get
            {
                if (_writer is null)
                {
                    lock (this)
                    {
                        if (_writer is null)
                        {
                            _writer = new StreamWriter(tcpClient.GetStream());
                        }
                    }
                }

                return _writer;
            }
        }

        private StreamReader _reader;
        internal StreamReader Reader
        {
            get
            {
                if (_reader is null)
                {
                    lock (this)
                    {
                        if (_reader is null)
                        {
                            _reader = new StreamReader(tcpClient.GetStream());
                        }
                    }
                }

                return _reader;
            }
        }
    
        #endregion

        public string Hostname { get; }
        public int Port { get; }
        public bool Connected => tcpClient.Connected;

        public Client(string hostname, int port)
        {
            Hostname = hostname ?? throw new ArgumentNullException(nameof(hostname));
            Port = port;
        }

        #region Connect
        #region Sync

        public void Connect() => tcpClient.Connect(Hostname, Port);
        public bool TryConnect()
        {
            bool success;

            try
            {
                Connect();
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        #endregion
        #region Async

        public async Task ConnectAsync() => await tcpClient.ConnectAsync(Hostname, Port);
        public async Task<bool> TryConnectAsync()
        {
            bool success;

            try
            {
                await ConnectAsync();
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        #endregion
        #endregion

        public void Close() => tcpClient.Close();

        public string Read() => Reader.ReadLine();

        public async Task<string> ReadAsync() => await Reader.ReadLineAsync();

        public void Write(string message)
        {
            Writer.WriteLine(message);
            Writer.Flush();
        }

        public async Task WriteAsync(string message)
        {
            await Writer.WriteLineAsync(message);
            await Writer.FlushAsync();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    tcpClient.Dispose();
                    _writer?.Dispose();
                    _reader?.Dispose();
                }

                disposedValue = true;
            }
        }


        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
