﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SeimSockets
{
    public partial class Server : IServer, IDisposable
    {
        private readonly ClientCollection _clients = new ClientCollection();

        public int Port { get; }

        public Server(int port)
        {
            Port = port;
        }

        #region Util

        private TcpListener GetListener() => TcpListener.Create(Port);

        #endregion

        #region Listen
        #region Sync

        public ClientHandler Listen()
        {
            var listener = GetListener();

            listener.Start();

            ClientHandler clientReference = listener.AcceptTcpClient();
            _clients.Add(clientReference);

            listener.Stop();

            return clientReference;
        }

        #endregion

        #region Async

        public async Task<ClientHandler> ListenAsync()
        {
            var listener = GetListener();

            listener.Start();

            ClientHandler clientReference = await listener.AcceptTcpClientAsync();
            _clients.Add(clientReference);

            listener.Stop();

            return clientReference;
        }

        #endregion
        #endregion

        #region Close

        public void Close(ClientHandler clientReference)
        {
            clientReference.Close();
        }

        public void CloseAll()
        {
            foreach (var client in _clients)
            {
                client.Close();
            }
        }

        #endregion

        #region Write
        #region Sync

        public void WriteToClient(ClientHandler clientReference, string data)
        {
            clientReference.Writer.WriteLine(data);
            clientReference.Writer.Flush();
        }

        public void WriteToAll(string data)
        {
            foreach (var client in _clients.Where(x => x.Connected))
            {
                client.Writer.WriteLine(data);
                client.Writer.Flush();
            }
        }

        #endregion

        #region Async

        public async Task WriteToClientAsync(ClientHandler clientReference, string data)
        {
            await clientReference.Writer.WriteLineAsync(data);
            await clientReference.Writer.FlushAsync();
        }

        public async Task WriteToAllAsync(string data)
        {
            foreach (var client in _clients)
            {
                await client.Writer.WriteLineAsync(data);
                await client.Writer.FlushAsync();
            }
        }

        #endregion
        #endregion

        #region Read
        #region Sync

        public string Read(ClientHandler clientReference)
        {
            string message = clientReference.Reader.ReadLine();
            return message;
        }

        #endregion

        #region Async

        public async Task<string> ReadAsync(ClientHandler clientReference)
        {
            string message = await clientReference.Reader.ReadLineAsync();
            return message;
        }

        #endregion
        #endregion

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _clients.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
