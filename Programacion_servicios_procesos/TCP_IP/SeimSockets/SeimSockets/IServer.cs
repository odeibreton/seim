﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SeimSockets
{
    public interface IServer
    {
        /// <summary>
        /// Obtiene el valor del puerto de escucha.
        /// </summary>
        int Port { get; }

        /// <summary>
        /// Cierra la conexión con un cliente.
        /// </summary>
        /// <param name="clientReference">Referencia al cliente.</param>
        void Close(ClientHandler clientReference);
        void CloseAll();
        ClientHandler Listen();
        Task<ClientHandler> ListenAsync();
        void WriteToAll(string data);
        Task WriteToAllAsync(string data);
        void WriteToClient(ClientHandler clientReference, string data);
        Task WriteToClientAsync(ClientHandler clientReference, string data);
        string Read(ClientHandler clientReference);
        Task<string> ReadAsync(ClientHandler clientReference);
    }
}