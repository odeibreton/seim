﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Servicios2.App_Code
{
    [ServiceContract]
    public interface IServicioConsultas
    {
        [OperationContract]
        DataTable GetData(string connectionString, string table);

        [OperationContract]
        int GetEmployeeID(string connectionString, string table, int salary);

        [OperationContract]
        DataTable GetCities(string country);
    }
}
